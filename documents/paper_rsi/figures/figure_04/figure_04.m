function figure_04
% FIGURE_04 - create figure 4 for iwave RSI paper draft
% 
% useage: figure_04;
%
% Ed Daw, 16th September 2020

% this figure is a 2x2 transfer function. Need a TF representation
% of the transfer function matrix

freq=1; % frequency in Hz
w=0.001;
srate=256;
delta=2*pi*(freq/srate);

% define transfer function

iwavesys=tf({(1-exp(-w))*[1 -exp(-w)*cos(delta)], (1-exp(-w))*[0 -exp(-w)*sin(delta)]; ...
       (1-exp(-w))*[0 exp(-w)*sin(delta)], (1-exp(-w))*[1 -exp(-w)*cos(delta)]}, ...
      {[1 -2*exp(-w)*cos(delta) exp(-2*w)],[1 -2*exp(-w)*cos(delta) exp(-2*w)]; ...
       [1 -2*exp(-w)*cos(delta) exp(-2*w)],[1 -2*exp(-w)*cos(delta) exp(-2*w)]}, ...
        1/256,'Variable','z^-1' );
    
transmat=[1+exp(-w) , (1-exp(-w))/tan(delta) ; (1-exp(-w))/tan(delta) , ...
              exp(-w)*( ((exp(w)-1)/sin(delta))^2 - 1 ) + 3 ];
          
iwavesysrot=transmat*iwavesys;
    
% measure transfer function
freq=logspace(-1,log10(srate/2),1000);
iwaveh=freqresp(iwavesysrot,freq,'Hz');

% make plots
fig=edstyle(1,1,16,16);
iwaveh11squeeze=squeeze(iwaveh(1,1,:));
iwaveh21squeeze=squeeze(iwaveh(2,1,:));
subplot(2,2,1);
loglog(freq,abs(iwaveh11squeeze),'-k');
axis([min(freq) max(freq) 1e-6 1]);
xlabel('frequency (Hz)');
xticks([0.1 1 10 100]);
xticklabels({'0.1','1','10','100'});
ylabel('|H_D(f)|');
grid on;
subplot(2,2,2); 
loglog(freq,abs(iwaveh21squeeze),'-k');
axis([min(freq) max(freq) 1e-6 1]);
xlabel('frequency (Hz)');
xticks([0.1 1 10 100]);
xticklabels({'0.1','1','10','100'});
ylabel('|H_Q(f)|');
grid on;
subplot(2,2,3);
semilogx(freq,180*angle(iwaveh11squeeze)/pi,'-k');
axis([min(freq) max(freq) -200 200]);
xlabel('frequency (Hz)');
xticks([0.1 1 10 100]);
xticklabels({'0.1','1','10','100'});
ylabel('phase lag D (deg)');
grid on;
subplot(2,2,4);
semilogx(freq,180*angle(iwaveh21squeeze)/pi,'-k');
axis([min(freq) max(freq) -200 200]);
xlabel('frequency (Hz)');
xticks([0.1 1 10 100]);
xticklabels({'0.1','1','10','100'});
ylabel('phase lag Q (deg)');
grid on;

