function fig=figure_01
% FIGURE_01 - generate the transfer function of iwave
% useage: <figure handle> = figure_01;
%
% Ed Daw, 22nd September 2016

% set up plot style
set(0,'DefaultAxesFontName','Helvetica');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultAxesFontWeight','normal');
% create figure
fig=edstyle(1,1,20,20);
% set up plot
chi=linspace(-pi,pi,10000);
freqhz=200;
samprate=1000;
Delta=ones(size(chi))*2*pi*freqhz/samprate;
w=1;
w2=1/10;
w3=1/100;
w4=1/1000;
A=(1-exp(-w))./ ...
    sqrt(ones(size(chi))-2*exp(-w)*cos(Delta-chi)+ ... 
    ones(size(chi))*exp(-2*w));
Phi=atan((exp(-w)*sin(Delta-chi))./ ...
    (1-exp(-w)*cos(Delta-chi)));
A2=(1-exp(-w2))./ ...
    sqrt(ones(size(chi))-2*exp(-w2)*cos(Delta-chi)+ ... 
    ones(size(chi))*exp(-2*w2));
Phi2=atan((exp(-w2)*sin(Delta-chi))./ ...
    (1-exp(-w2)*cos(Delta-chi)));
A3=(1-exp(-w3))./ ...
    sqrt(ones(size(chi))-2*exp(-w3)*cos(Delta-chi)+ ... 
    ones(size(chi))*exp(-2*w3));
Phi3=atan((exp(-w3)*sin(Delta-chi))./ ...
    (1-exp(-w3)*cos(Delta-chi)));
A4=(1-exp(-w4))./ ...
    sqrt(ones(size(chi))-2*exp(-w4)*cos(Delta-chi)+ ... 
    ones(size(chi))*exp(-2*w4));
Phi4=atan((exp(-w4)*sin(Delta-chi))./ ...
    (1-exp(-w4)*cos(Delta-chi)));
subplot(2,1,1);
semilogy(chi,A,'-k');
axis([-pi pi 1e-4 2])
hold on;
semilogy(chi,A2,'-k');
semilogy(chi,A3,'-k');
semilogy(chi,A4,'-k');
ylabel('A');
grid on;
subplot(2,1,2);
plot(chi,180*Phi/pi,'-k');
axis([-pi pi -100 100]);
hold on;
plot(chi,180*Phi2/pi,'-k');
plot(chi,180*Phi3/pi,'-k');
plot(chi,180*Phi4/pi,'-k');
ylabel('\Phi (degrees)');
xlabel('frequency (radians per sample)');
grid on;

