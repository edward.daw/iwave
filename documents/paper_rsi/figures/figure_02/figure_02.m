function figure_02 
% FIGURE_02 - make the response to a sine wave drive for the iwave paper
%
% Ed Daw - 4th September 2019

% put input parameters in workspace
delta=0.30680;
w=0.3;

% run the simulink model
sim('iwave_core.slx',0.013);

% generate sample index array
nsamples=length(tout);
finergrain=10;
n=linspace(0,nsamples-1,nsamples*finergrain);

% generate limiting ellipse
displayout=sprintf('Delta was %.3f, w was %.3f',delta,w);
disp(displayout);
af=1;
ab=(1-exp(-w))/sqrt(1-2*exp(-w)*cos(2*delta)+exp(-2*w));
pf=0;
pb=atan(exp(-w)*sin(2*delta)/(1-exp(-w)*cos(2*delta)));
alpha=(pf-pb)/2;
beta=(pf+pb)/2;
ylim=0.5*exp(1i*beta)*((af+ab)*cos(n*delta+alpha*ones(size(n)))+ ...
    1i*(af-ab)*sin(n*delta+alpha*ones(size(n))));

% make the plot
% set default figure styles
set(0,'DefaultAxesFontName','Helvetica');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultAxesFontWeight','normal');
fig1=figure;
% set the figure size
framesize1=[0.1 0.1 0.5 0.5];
set(fig1,'PaperType','a4');
set(fig1,'PaperUnits','normalized');
set(fig1,'PaperPosition',framesize1);
plot(dout,qout,'k.',real(ylim),imag(ylim),'k-','MarkerSize',16);
ax=gca;
ax.XTick = [-0.8 -0.6 -0.4 -0.2 0 +0.2 +0.4 +0.6 +0.8];
ax.YTick = [-0.8 -0.6 -0.4 -0.2 0 +0.2 +0.4 +0.6 +0.8];
axis square;
set(gca,'GridColor',[0 0 0]);
set(gca,'GridAlpha',1);
set(gca,'GridLineStyle',':');
axislimits=0.8*[-1.0 +1.0 -1.0 +1.0];
axis(axislimits);
xlabel('real(y_n)');
ylabel('imag(y_n)');
grid on;
print -dpdf figure_02.pdf;
