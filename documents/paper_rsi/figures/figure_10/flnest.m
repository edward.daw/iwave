function flnest
% FLNEST - try and track some lines in LIGO data
%
% Ed Daw and Elliot Jones, e.daw@sheffield.ac.uk, 30th October 2020

srate=16384;
duration=4096;
filestart=1164558336;

% ingest data
[fulldata,fulltsamp]=frgetvect( ...
    '../public_ligo_data/H-H1_GWOSC_O2_16KHZ_R1-1164558336-4096.gwf', ...
    'H1:GWOSC-16KHZ_R1_STRAIN',filestart,duration);

% plot(fulltsamp,fulldata,'-');

lockstart=2263;
lockend=3056;
firstsample=lockstart*srate;
lastsample=lockend*srate;

data=double(fulldata(firstsample+1:lastsample));
time=double(fulltsamp(firstsample+1:lastsample));
clear fulldata;
clear fulltsamp;
size(data);

save framedata.mat;
