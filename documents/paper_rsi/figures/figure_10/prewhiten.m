function data_w7=prewhiten(data)
% PREWHITEN - prewhiten the data ingested from raw frames
%
% Ed Daw, 2nd December 2020

% sampling rate
srate=16384;

% initial 4th order butterworth highpass
fcutoff_hz=30;
[bd1,ad1]=butter(4,fcutoff_hz/(srate/2),'high');

% slight adjustment to slope above 30Hz - want a single zero at 30Hz
% really
fknee=10;
kneefilter=zpk(-2*pi*fknee,-2*pi*srate,2*pi*fknee);
kneefilterdig=c2d(kneefilter,1/srate,'Tustin');
[bd1a,ad1a]=tfdata(kneefilterdig,'v');

% next stage cheby2 bandpass
edgelow=5;
edgehigh=300;
% define passband edges as the ratio of flow and fhigh to the nyquist
% frequency
frange=[edgelow edgehigh]/(srate/2);
% make a 3rd order digital chebyshev 2 filter at the required sampling rate
% having a 50dB stopband attenuation.
[bd2,ad2]=cheby2(3, 50, frange);

% first apply the butterworth highpass filter
[data_w,zout_w]=filter(bd1,ad1,data);
[data_w2,~]=filter(bd1,ad1,data_w,zout_w);
clear data_w;

% next apply the chebyshev 2 bandpass filter
[data_w3,z1]=filter(bd2,ad2,data_w2);
clear data_w2;
[data_w4,z2]=filter(bd2,ad2,data_w3,z1);
clear data_w3;
[data_w5,z3]=filter(bd2,ad2,data_w4,z2);
clear data_w4;
[data_w6,~]=filter(bd2,ad2,data_w5,z3);

% next apply knee filter adjustment
data_w7=filter(bd1a,ad1a,data_w6);

% next apply the passband slope remover

% now plot the amplitude spectral density
[v,f]=nsd_pwelch(data_w7,50,srate);
loglog(f,v,'-k');



