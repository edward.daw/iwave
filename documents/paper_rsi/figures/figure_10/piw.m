function output_data=piw(predata)
% PIW - process with iwave
%
% useage: piw(<whitened input data>)

srate=16384;
tau=13;
%tau=3;
frequencies=[35.7 36.7 37.3 40.9 42.9 46.1 60.0 69.6];
% initialise iwave
fstate=iwavePllArrayConstruct(length(frequencies), ...
    srate, tau, 0, srate/2, frequencies);

% run iwave over data
[output_data, output_freq, output_amp, output_err, output_state] = ...
    iwavePllArrayRun(predata,fstate);

% plot the frequencies
len=length(output_data);
freq1=output_freq(1:len);
freq2=output_freq(len+1:2*len);
freq3=output_freq(2*len+1:3*len);
freq4=output_freq(3*len+1:4*len);
freq5=output_freq(4*len+1:5*len);
freq6=output_freq(5*len+1:6*len);
freq7=output_freq(6*len+1:7*len);
freq8=output_freq(7*len+1:8*len);
amp1=output_amp(1:len);
amp2=output_amp(len+1:2*len);
amp3=output_amp(2*len+1:3*len);
amp4=output_amp(3*len+1:4*len);
amp5=output_amp(4*len+1:5*len);
amp6=output_amp(5*len+1:6*len);
amp7=output_amp(6*len+1:7*len);
amp8=output_amp(7*len+1:8*len);
err1=output_err(1:len);
err2=output_err(len+1:2*len);
err3=output_err(2*len+1:3*len);
err4=output_err(3*len+1:4*len);
err5=output_err(4*len+1:5*len);
err6=output_err(5*len+1:6*len);
err7=output_err(6*len+1:7*len);
err8=output_err(7*len+1:8*len);
time=linspace(0,(len-1)/srate,len);

% create scaled figure
fig=edstyle(1,1,16,28);

% first frequency

subplot(6,2,1);
plot(time,freq2,'-k');
xlim([0 max(time)]);
title('36.7 Hz');
ylabel('frequency (Hz)');
subplot(6,2,3);
semilogy(time,amp2,'-k');
xlim([0 max(time)]);
ylabel('amplitude');
subplot(6,2,5);
werr2=err2.*amp2/std(predata);
plot(time,werr2,'-k');
xlabel('time (s)');
ylabel('scaled error');
axis([0 max(time) -3 3]);

% second frequency

subplot(6,2,2);
plot(time,freq3,'-k');
xlim([0 max(time)]);
title('37.3 Hz');
ylabel('frequency (Hz)');
subplot(6,2,4);
semilogy(time,amp3,'-k');
xlim([0 max(time)]);
ylabel('amplitude');
subplot(6,2,6);
werr3=err3.*amp3/std(predata);
plot(time,werr3,'-k');
xlabel('time (s)');
ylabel('scaled error');
axis([0 max(time) -3 3]);

% third frequency

subplot(6,2,7);
plot(time,freq5,'-k');
xlim([0 max(time)]);
title('42.9 Hz');
ylabel('frequency (Hz)');
subplot(6,2,9);
semilogy(time,amp5,'-k');
xlim([0 max(time)]);
ylabel('amplitude');
subplot(6,2,11);
werr5=err5.*amp5/std(predata);
plot(time,werr5,'-k');
xlabel('time (s)');
ylabel('scaled error');
axis([0 max(time) -3 3]);

% fourth frequency 

subplot(6,2,8);
plot(time,freq6,'-k');
xlim([0 max(time)]);
title('46.1 Hz');
ylabel('frequency (Hz)');
subplot(6,2,10);
semilogy(time,amp6,'-k');
ylabel('amplitude');
xlim([0 max(time)]);
subplot(6,2,12);
werr6=err6.*amp6/std(predata);
plot(time,werr6,'-k');
xlabel('time (s)');
ylabel('scaled error');
axis([0 max(time) -3 3]);

% make figure
print -dpdf figure11.pdf

% figure;
% subplot(3,1,1);
% plot(time,freq5,'-');
% title('42.9 Hz');
% subplot(3,1,2);
% semilogy(time,amp5,'-');
% subplot(3,1,3);
% werr5=err5.*amp5/std(predata);
% plot(time,werr5,'-');
% axis([0 max(time) -3 3]);
% figure;
% subplot(3,1,1);
% plot(time,freq6,'-');
% title('46.1 Hz');
% subplot(3,1,2);
% semilogy(time,amp6,'-');
% subplot(3,1,3);
% werr6=err6.*amp6/std(predata);
% plot(time,werr6,'-');
% axis([0 max(time) -3 3]);
% figure;
% subplot(3,1,1);
% plot(time,freq7,'-');
% title('60.0 Hz');
% subplot(3,1,2);
% semilogy(time,amp7,'-');
% subplot(3,1,3);
% werr7=err7.*amp7/std(predata);
% plot(time,werr7,'-');
% axis([0 max(time) -3 3]);
% figure;
% subplot(3,1,1);
% plot(time,freq8,'-');
% title('69.6 Hz');
% subplot(3,1,2);
% semilogy(time,amp8,'-');
% subplot(3,1,3);
% werr8=err8.*amp8/std(predata);
% plot(time,werr8,'-');
% axis([0 max(time) -3 3]);

% make a figure showing cumulative amplitude spectral densities
chopsecs=50;
chopfiltdata=predata(chopsecs*16384:length(predata));
chopdataout=output_data(chopsecs*16384:length(output_data));
[vpre,fpre]=nsd_pwelch(chopdataout,50,srate);
[vpost,fpost]=nsd_pwelch(chopfiltdata,50,srate);
figure;
vprecum=sqrt(cumsum(vpre.*vpre));
vpostcum=sqrt(cumsum(vpost.*vpost));
semilogx(fpre,vprecum,'-',fpost,vpostcum,'-k');
figure;
semilogx(fpre,vpre,'-',fpost,vpost,'-k');