function [freqout,hout]=iwaveamod(tau)
% IWAVEAMOD - measure the response of iwave to amplitude modulation at
% a predetermined reaction timescale
%
% useage: [freq,h]=iwaveamod(<response time (s)>);
%
% steps to simulate response of iwave model to sine wave amplitude 
% modulation. Ed Daw, 9th April 2015
ts=1/16384;
% response time expected from filter (seconds)
resptime=tau;
% number of periods for transfer function measurement for input frequency
npmeas=2;
% vector of frequencies for transfer function
freq=logspace(-1.5,1.5,20);
% rounding of frequency vector required for fixed sample time test
per=1./freq;
perround=ts*floor(per/ts);
freqround=1./perround;
% create excitation waveform
inwave=frest.createFixedTsSinestream(ts,2*pi*freqround);
% turn filter on model output off; this signal is sufficiently clean
% not to need it.
inwave.ApplyFilteringInFRESTIMATE='off';
% turn off ramping of excitation waveform between frequencies; no need
% for this as long as settling time is sufficient for each test frequency
inwave.RampPeriods=0;
inwave.NumPeriods=ceil(2*resptime./per)+npmeas;
inwave.SettlingPeriods=ceil(2*resptime./per);
% amplitude of excitation waveform
inwave.Amplitude=0.1;
% load model
%testmodel7;
% make string containing response time
resptimestring=sprintf('%.2f',tau);
% adjust model
modelname='testmodel7'
fbh=getSimulinkBlockHandle([modelname '/Time constant'],true);
set_param(fbh,'Value',resptimestring);
set_param(modelname,'SimulationCommand','Update');
% get state vector for initial states of each model component having them
op=operpoint(modelname);
% set input and output points for excitation
io(1)=linio([modelname '/AMInput'],1,'openinput');
io(2)=linio([modelname '/DebiasOut'],1,'openoutput');
% run the model
[sysest,simout]=frestimate(modelname,op,io,inwave);
% set simview options to include phase shift
opt=frest.simViewOptions('SummaryPhaseVisible','on');
% open simview, view results to check that the waveform was stable for the
% transfer function measurement
% frest.simView(simout,inwave,sysest,opt);
% extract results
freqout=squeeze(sysest.Frequency)/(2*pi);
hout=squeeze(sysest.ResponseData);

