function fig=figure_05
% FIGURE5 - a model of the amplitude modulation characeristics
% of iwave in open loop.
%
% Ed Daw, 22nd June 2017

% time constant sets
tauset=[10 3 1 0.3 0.1];
% set up plot style
set(0,'DefaultAxesFontName','Helvetica');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultAxesFontWeight','normal');
fig=figure;
% set the figure size
framesize1=[0.1 0.1 0.8 0.5];
set(fig,'PaperType','a4');
set(fig,'PaperUnits','normalized');
set(fig,'PaperPosition',framesize1);
hold on;
for taucount=1:length(tauset),
    trackit=sprintf('Analyzing tau=%.2f',tauset(taucount));
    disp(trackit);
    [f,h]=iwaveamod(tauset(taucount));
    subplot(2,1,1);
    loglog(f,abs(h),'k-');
    hold on;
    xlabel('frequency (Hz)');
    ylabel('|H(f)|');
    subplot(2,1,2);
    semilogx(f,180*angle(h)/pi,'k-');
    hold on;
    xlabel('frequency (Hz)'); 
    ylabel('arg[H(f)] (deg)');
end
subplot(2,1,1);
axis([3e-2 30 1e-2 1]);
grid on;
set(gca,'GridColor',[0 0 0]);
set(gca,'GridAlpha',1);
set(gca,'GridLineStyle',':');
set(gca,'MinorGridColor',[0 0 0]);
set(gca,'MinorGridAlpha',0.5);
set(gca,'MinorGridLineStyle',':');
subplot(2,1,2);
axis([3e-2 30 -90 0]);
grid on;
set(gca,'GridColor',[0 0 0]);
set(gca,'GridAlpha',1);
set(gca,'GridLineStyle',':');
set(gca,'MinorGridColor',[0 0 0]);
set(gca,'MinorGridAlpha',0.5);
set(gca,'MinorGridLineStyle',':');
print -dpdf figure_05.pdf
