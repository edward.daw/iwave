function fig=figure7
% FIGURE 7 - generate figure 7 of the iwave paper
%
% useage: [f1,h1,f2,h2,f3,h3,f4,h4]=figure7;
% Ed Daw, 8th May 2015

% set up plot style
set(0,'DefaultAxesFontName','Helvetica');
set(0,'DefaultAxesFontSize',12);
set(0,'DefaultAxesFontWeight','normal');
fig=figure;
% set the figure size
framesize1=[0.1 0.1 0.8 0.5];
set(fig,'PaperType','a4');
set(fig,'PaperUnits','normalized');
set(fig,'PaperPosition',framesize1);
hold on;

% run simulink
[f1,h1]=frphasepe2(3.0,59.79562);
[f2,h2]=frphasepe2(1.0,59.79562);
[f3,h3]=frphasepe2(0.3,59.79562);
[f4,h4]=frphasepe2(0.1,59.79562);

% make plots
subplot(2,1,1);
loglog(f1,abs(h1),'-k', ...
    f2,abs(h2),'-k', ...
    f3,abs(h3),'-k', ...
    f4,abs(h4),'-k');
xlabel('frequency (Hz)');
ylabel('|H(f)|');
grid on;
subplot(2,1,2);
semilogx(f1,180*angle(h1)/pi,'-k', ...
    f2,180*angle(h2)/pi,'-k', ...
    f3,180*angle(h3)/pi,'-k', ...
    f4,180*angle(h4)/pi,'-k');
grid on;
xlabel('frequency (Hz)');
ylabel('phase (degrees)');

% print results
print -dpdf figure7_raw.pdf

