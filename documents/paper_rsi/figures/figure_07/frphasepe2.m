function[freqout,hout]=frphasepe2(tau,carrierfreq)
% FRPHASEPE2 - return the frequency response of the iwave phase detector
% This version incorporates parallel execution.
% Ed Daw, 6th May 2015
% useage: [<vector of frequencies>, <complex freq response vector>]= ...
%              frphasepe(<iwave time constant>, <carrier frequency>);

% start parallel engine if it isn't started already
if isempty(gcp)
    parpool local;
end
% steps to simulate response of iwave model to sine wave amplitude 
% modulation. Ed Daw, 9th April 2015
ts=1/16384;
% response time expected from filter (seconds)
resptime=tau;
% number of periods for transfer function measurement for input frequency
npmeas=2;
% vector of frequencies for transfer function
freq=logspace(-1.5,1.5,20);
% freq = logspace(-1,-0.2,20);
% rounding of frequency vector required for fixed sample time test
per=1./freq;
perround=ts*floor(per/ts);
freqround=1./perround;
% create excitation waveform
inwave=frest.createFixedTsSinestream(ts,2*pi*freqround);
% turn filter on model output off; this signal is sufficiently clean
% not to need it.
inwave.ApplyFilteringInFRESTIMATE='on';
% set mode to one-at-a-time to allow parallel processing
inwave.SimulationOrder='OneAtATime';
% turn off ramping of excitation waveform between frequencies; no need
% for this as long as settling time is sufficient for each test frequency
inwave.RampPeriods=0;
% total number of periods of each wave
% inwave.NumPeriods=[3 3 3 9 18];
% number of periods of settling before response is measured
% inwave.SettlingPeriods=[1 1 1 7 16];
% the +3 on this line is to allow for settling of the bandpass filter;
inwave.NumPeriods=ceil(6*resptime./per)+npmeas+3;
inwave.SettlingPeriods=ceil(6*resptime./per);
% amplitude of excitation waveform
%inwave.Amplitude=0.02*2*pi;
inwave.Amplitude=0.02;
% set model name
modelname='phasemod2';
% adjust model time constant
resptimehandle=getSimulinkBlockHandle([modelname '/iWaveResponseTime'],true);
resptimestring=sprintf('%.2f',tau);
set_param(resptimehandle,'Value',resptimestring);
set_param(modelname,'SimulationCommand','Update');
% adjust model carrier frequency
% fchandle=getSimulinkBlockHandle('phasemod2/iWaveCarrier',true);
% fcstring=sprintf('59.79562');
% fcstring=sprintf('%.8f',carrierfreq);
% set_param(fchandle,'Value',fcstring);
% set_param('phasemod2','SimulationCommand','Update');
% get state vector for initial states of each model component having them
op=operpoint(modelname);
% set input and output points for excitation
io(1)=linio([modelname '/PMInput'],1,'openinput');
io(2)=linio([modelname '/Divide2'],1,'openoutput');
% set up parallel options
options=frestimateOptions('UseParallel','on');
% run the model
[sysest,simout]=frestimate(modelname,op,io,inwave,options);
% set simview options to include phase shift and to parallelise
% the simulation process.
opt=frest.simViewOptions('SummaryPhaseVisible','on');
% open simview, view results to check that the waveform was stable for the
% transfer function measurement
frest.simView(simout,inwave,sysest,opt);
% extract results
freqout=squeeze(sysest.Frequency)/(2*pi);
hout=squeeze(sysest.ResponseData);
% make plot
% fig=edstyle([1 1 21 21]);
% subplot(2,1,1);
% loglog(freqout,abs(hout),'-');
% xlabel('frequency (Hz)');
% ylabel('|H(f)|');
% grid on;
% subplot(2,1,2);
% semilogx(freqout,180*angle(hout)/pi,'-');
% grid on;
% xlabel('frequency (Hz)');
% ylabel('phase (degrees)');
