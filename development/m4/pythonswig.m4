dnl  ====================================================
dnl             PYTHON and SWIG support checks
dnl  ====================================================

AC_DEFUN([CHECK_PYTHONSWIG],
[
AC_ARG_WITH(python,
[  --with-python=DIR    the directory where Python is installed ],
PYTHON_DIR=${withval},
PYTHON_DIR=)

if test -n "${PYTHON_DIR}"
then
    pythonswig=true
else
    pythonswig=false
fi

])

