dnl  ====================================================
dnl             MATLAB library macros
dnl  ====================================================

dnl
dnl  CHECK_MATLAB - find Matlab libraries
dnl  ----------------------------------------------------
AC_DEFUN([CHECK_MATLAB],
[
AC_ARG_WITH(matlab,
[  --with-matlab=DIR	the root directory of your MATLAB installation. If the matlab you wish to use is on your path, just --with-matlab will work. ],
MATLAB_DIR=${withval},
MATLAB_DIR=)

if test -n "${MATLAB_DIR}"
dnl CASE WHERE THE ROOT DIRECTORY FOR MATLAB WAS GIVEN
then
    AC_MSG_CHECKING(for MATLAB mex command)
dnl check that matlab in the directory given is executable. If it is, then execute it
dnl and check that it behaves as matlab should, albeit superficially 
    if test -x ${MATLAB_DIR}/bin/matlab
    then
dnl	try running matlab -n and analysing what comes back, looking for the install path
	MATLABCOMMAND=${MATLAB_DIR}/bin/matlab
	MATLABROOT=$($MATLABCOMMAND -n | grep TOOLBOX | sed 's/[[^=]]*=\ \([[^$]]*\)\/toolbox$/\1/')
	if test -x $MATLABROOT/bin/mex
	then
	    MEXCOMMAND=$MATLABROOT/bin/mex
	    AC_SUBST(MEXCOMMAND)
	    AC_MSG_RESULT([${MEXCOMMAND}])
	    havemex=true
	else
	    AC_MSG_RESULT([no])
            havemex=false
        fi    
dnl CASE WHERE THE ROOT DIRECTORY FOR MATLAB WAS NOT GIVEN
    else
	MATLABCOMMAND=$(which matlab)
	MATLABROOT=$($MATLABCOMMAND -n | grep TOOLBOX | sed 's/[[^=]]*=\ \([[^$]]*\)\/toolbox$/\1/')
	if test -x $MATLABROOT/bin/mex
	then
            MEXCOMMAND=$MATLABROOT/bin/mex
	    AC_SUBST(MEXCOMMAND)
	    AC_MSG_RESULT([${MEXCOMMAND}])
	    havemex=true
	else
	    AC_MSG_RESULT([no])
	    havemex=false
	fi
    fi
else
    AC_MSG_RESULT([matlab wrapper will not be built.])
    havemex=false
fi		

])

dnl
dnl  CHECK MEXEXT - figure out the extension for MATLAB mex files
dnl  ------------------------------------------------------------
AC_DEFUN([CHECK_MEXEXT],
[
AC_ARG_WITH(matlab,
[  --with-matlab=DIR    the root directory of your MATLAB installation. If the matlab you wish to use is on your path, just --with-matlab will work. ],
MATLAB_DIR=${withval},
MATLAB_DIR=)

if test -n "${MATLAB_DIR}"
dnl CASE WHERE THE ROOT DIRECTORY FOR MATLAB WAS GIVEN
then
    AC_MSG_CHECKING(for MATLAB mexext command )
dnl check that matlab in the directory given is executable. If it is, then execute it
dnl and check that it behaves as matlab should, albeit superficially
    if test -x ${MATLAB_DIR}/bin/mexext
    then
dnl     try running matlab -n and comb through what comes back for the install path
	MATLABCOMMAND=${MATLAB_DIR}/bin/matlab
	MATLABROOT=$($MATLABCOMMAND -n | grep TOOLBOX | sed 's/[[^=]]*=\ \([[^$]]*\)\/toolbox$/\1/')
	if test -x $MATLABROOT/bin/mexext
	then	
	    MEXEXTCOMMAND=$MATLABROOT/bin/mexext
	    AC_MSG_RESULT([${MEXEXTCOMMAND}])
	    AC_SUBST(MEXEXTCOMMAND)
	    havemexext=true
        else
	    AC_MSG_RESULT([no])
	    havemexext=false
        fi
dnl CASE WHERE THE ROOT DIRECTORY FOR MATLAB WAS NOT GIVEN
    else
	MATLABCOMMAND=$(which matlab)
	MATLABROOT=$($MATLABCOMMAND -n | grep TOOLBOX | sed 's/[[^=]]*=\ \([[^$]]*\)\/toolbox$/\1/')
	if test -x $MATLABROOT/bin/mexext
	then
	    MEXEXTCOMMAND=$MATLABROOT/bin/mexext
	    AC_SUBST(MEXEXTCOMMAND)
	    AC_MSG_RESULT([${MEXEXTCOMMAND}])
	    havemexext=true
	else
	    AC_MSG_RESULT([no])
	    havemexext=false
	fi
    fi
else
    AC_MSG_RESULT([cannot get extension for mex files. Matlab wrapper build disabled.])
    havemexext=false
fi		

])

