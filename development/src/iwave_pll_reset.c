#include <iwave_definitions.h>
#include <iwave_pll_reset.h>
#include <iwave_pll_construct.h>
#include <iwave_state_data_structure.h>

int iwave_pll_reset(double freq, double* pfstateout) {
    statedata* ps;
    double tau;
    
    /* make abbreviated pointer name to state data */
    ps=(statedata*)pfstateout;

    /* perform reset by re-running constructor */
    tau=1/(ps->w * ps->srate);
    iwave_pll_construct(ps->srate, tau, freq, 0.0,
        pfstateout );
    
    return 0;
}