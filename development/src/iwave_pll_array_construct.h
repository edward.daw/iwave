#ifndef _IWAVE_PLL_ARRAY_CONSTRUCT
#define _IWAVE_PLL_ARRAY_CONSTRUCT


#ifdef __cplusplus
extern "C" {
#endif

int iwave_pll_array_construct(int nfilters,
                              double fs,
                              double tau,
                              double fmin,
                              double fmax,
                              double farray[],
                              double fstate[]);

#ifdef __cplusplus
}
#endif

#endif /* declaration of _IWAVE_PLL_ARRAY_CONSTRUCT */

