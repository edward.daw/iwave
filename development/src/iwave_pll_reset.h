#ifndef _IWAVE_PLL_RESET
#define _IWAVE_PLL_RESET

#ifdef __cplusplus
extern "C" {
#endif

int iwave_pll_reset(double freq, double* pfstateout);

#ifdef __cplusplus
}
#endif

#endif /* declaration of _IWAVE_PLL_RESET */ 