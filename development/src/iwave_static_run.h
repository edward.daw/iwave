#ifndef _IWAVE_STATIC_RUN
#define _IWAVE_STATIC_RUN

#ifdef __cplusplus
extern "C" {
#endif

int iwave_static_run(int datalength,
                     double pindata[],
                     double pdout[],
                     double pqout[],
                     double pfstateout[]);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef _IWAVE_STATIC_RUN */
