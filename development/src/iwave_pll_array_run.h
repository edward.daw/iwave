#ifndef _IWAVE_PLL_ARRAY_RUN
#define _IWAVE_PLL_ARRAY_RUN

#ifdef __cplusplus
extern "C" {
#endif
    
int iwave_pll_array_run(int ndata, 
        double indata[], 
        double outdata[],
        double ampdata[],
        double errordata[],
        double freqdata[], 
        double fstate[]);
    
#ifdef __cplusplus
}
#endif
    
#endif /* declaration of _IWAVE_PLL_ARRAY_RUN */
