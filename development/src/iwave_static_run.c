#include <iwave_definitions.h>
#include <math.h>

int iwave_static_run(int datalength, 
                     double pindata[],
                     double pdout[],
                     double pqout[],
                     double pfstateout[]) {

    int dcount,foundnan=0;
    double yr,yi,yrm,yim;
    
    /* load the previous output elements */
    yrm=pfstateout[6];
    yim=pfstateout[7];
    
    /* run the filter over the input data */
    for(dcount=0;dcount<datalength;++dcount) {
        /* check for NANs in the input data */
        /* replace with zeros and set flag so user can be warned */
        if(isnan(pindata[dcount])!=0) {
            pindata[dcount]=0;
            foundnan=1;
        }
        yr=pfstateout[0]*yrm-pfstateout[1]*yim+pfstateout[2]*pindata[dcount];
        yi=pfstateout[1]*yrm+pfstateout[0]*yim;
        pdout[dcount] = pfstateout[3]*yr+pfstateout[4]*yi;
        pqout[dcount] = pfstateout[4]*yr+pfstateout[5]*yi;
        yrm=yr;
        yim=yi;
    }
    
    /* update the previous output elements */
    pfstateout[6]=yr;
    pfstateout[7]=yi;

    if(foundnan == 0) {
        return 0;
    } else {
        return 1;
    }
}