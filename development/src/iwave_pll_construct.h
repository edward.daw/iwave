#ifndef _IWAVE_PLL_CONSTRUCT
#define _IWAVE_PLL_CONSTRUCT

#ifdef __cplusplus
extern "C" {
#endif
    
int iwave_pll_construct(double fs_in,
			 double tau_in,
			 double fline_in,
			 double starttime,
			 double fstate[]);
    
#ifdef __cplusplus
}
#endif

#endif /* declaration of _IWAVE_PLL_CONSTRUCT */
