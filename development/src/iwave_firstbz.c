/* function to fold a frequency in radians per sample into      */
/* the 'first Brillouin zone' range between 0Hz and the Nyquist */
/* cutoff. Ed Daw, 1st March 2021                               */
#include <math.h>
#include <iwave_definitions.h>
#include <iwave_firstbz.h>

double iwave_firstbz(double deltain) {
    double deltaped;
    deltaped=deltain+IWAVEPI;
    return fabs(deltaped-
      2*IWAVEPI*floor(deltaped/(2*IWAVEPI))-IWAVEPI);
}