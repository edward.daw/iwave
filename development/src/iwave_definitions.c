#include <iwave_definitions.h>

double iwave_pi(void) {
    return (double)IWAVEPI;
}

int iwave_static_size(void) {
    return (int)STATESIZE;
}

int iwave_pll_size(void) {
    return (int)PLLSTATESIZE;
}

int iwave_array_header_size(void) {
    return (int)PLLARRAYDATASIZE;
}
