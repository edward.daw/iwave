#include <iwave_definitions.h>
#include <math.h>

int iwave_static_construct(double fs_in,
			 double tau_in,
			 double fline_in,
			 double fstate[]) {

  /* gain set for maximally flat response - see labbook page 137 */
  double delta,w,absq;
  w=1/(fs_in*tau_in);
  delta=2*IWAVEPI*fline_in/fs_in;
  absq = (1-exp(-w))*(1-exp(-w))/(1-2*exp(-w)*cos(2*delta)+exp(-2*w));
  fstate[0] = exp(-w)*cos(delta);  
  fstate[1] = exp(-w)*sin(delta);
  fstate[2] = 1-exp(-w);
  /* r11 = 1 + exp(-w) */
  fstate[3] = 2-fstate[2];
  /* r12 = r21 = (exp(-w)-1)/(tan(\Delta)) */
  fstate[4] = -fstate[2]*fstate[0]/fstate[1];
  /* r22 = ( 4/(1-a^2) + r12^2 ) / r11 */ 
  fstate[5] = (4/(1-absq)+fstate[4]*fstate[4])/fstate[3];
  fstate[6] = 0;
  fstate[7] = 0;
  /* initialise double-f static icwave */ 
  return 0;
}