#include <math.h>
#include <iwave_definitions.h>
#include <iwave_pll_construct.h>
#include <iwave_pll_array_construct.h>

int iwave_pll_array_construct(int nfilters,
                              double fs,
                              double tau,
                              double fmin,
                              double fmax,
                              double farray[],
                              double fstate[]) {
                              
   double fcurrent,df;
   double* pstate;
   int fcount;
   
   /* calculate interval between regularly spaced frequency guesses */
   if(nfilters>1) {
       df=(fmax-fmin)/(nfilters-1);
   } else {
       df=(fmax-fmin);
   }
   /* put the number of filters and the sampling rate in the state data */
   fstate[0]=(double)nfilters;
   fstate[1]=fs;
   fstate[2]=fmin;
   fstate[3]=fmax;
   /* loop over filter constructors */
   for(fcount=0;fcount<nfilters;++fcount) {
       /* calculate start address of state data for current filter */
       pstate=fstate+(int)PLLARRAYDATASIZE+
               fcount*(int)PLLSTATESIZE;
       /* calculate frequency of current filter */
       fcurrent=farray[fcount];
       /* call single filter constructor */
       iwave_pll_construct(fs, tau, fcurrent, 0, pstate);
   }
 
return 0;
}