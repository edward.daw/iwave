#ifndef _IWAVE_PLL_RUN
#define _IWAVE_PLL_RUN

#ifdef __cplusplus
extern "C" {
#endif
    
int iwave_pll_run(int ndata, 
                  double pindata[],
                  double pdout[],
                  double pqout[],
                  double paout[],
                  double peout[],
                  double pfreq[],
                  double pfstate[]);
    
#ifdef __cplusplus
}
#endif

#endif /* declaration of _IWAVE_PLL_RUN */                  
