#ifndef _IWAVE_PLL_ARRAY_REORDER
#define _IWAVE_PLL_ARRAY_REORDER

#ifdef __cplusplus
extern "C" {
#endif

int iwave_pll_array_reorder(int neworder[], double* fstatein, double* fstateout);

#ifdef __cplusplus
}
#endif

#endif /* declaration of _IWAVE_PLL_ARRAY_REORDER */
