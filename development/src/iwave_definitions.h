/* header for iwave complex IIR line filter */
/* Ed Daw, created 12th August 2011         */
/* eddaw1@gmail.com                         */

#ifndef _IWAVE_DEFINITIONS_H
#define _IWAVE_DEFINITIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#define IWAVEPI 3.141592653589793238
#define STATESIZE 8
/* these used by windows / mex software */
#define PLLSTATESIZE 17
#define PLLARRAYDATASIZE 4
/* end of defines for windows / mex software */
#define ICWAVESTATESIZE 5
#define PHASESIZE 14
#define PLLSIZE (2*STATESIZE+PHASESIZE)
#define ICWAVEPLLSIZE 17
#define IWAVECROSSPREWRITEPARAMS 6
  /*nfilters, nsamples, flower, fupper, srate, tau */
#define IWAVECROSSWRITEPARAMS 7
  /* keep_amp, keep_err, keep_freq, keep_d, keep_q, keep_df, keep_pred */
#define IWAVECROSSGLOBALDATASIZE \
  (IWAVECROSSPREWRITEPARAMS + IWAVECROSSWRITEPARAMS) 
#define IWAVECROSSCONTROLSIZE 1 /* not outputs, but per-filter controls */
#define IWAVECROSSOUTPUTDATAPERFILTERSIZE 7 /*amp,error,freq,i,q,df,pred*/
#define IWAVECROSSPERFILTERSIZE (PLLSIZE+ \
    IWAVECROSSOUTPUTDATAPERFILTERSIZE+ \
	IWAVECROSSCONTROLSIZE)

/* return the value of pi used by iwave */
double iwave_pi(void);

/* return the number of elements in the state data for iwave_static */
int iwave_static_size(void);

/* return the number of elements in the state data for iwave_pll */
int iwave_pll_size(void);

/* return the number of elements in the header for iwave_pll_array */
int iwave_array_header_size(void);

/* state vector for the phase locked tracker */
typedef enum {loopopen, loopclosed} feedbackstate; 
  
#ifdef __cplusplus
}
#endif

#endif /* _IWAVE_DEFINITIONS_H */
