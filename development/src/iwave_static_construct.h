#ifndef _IWAVE_STATIC_CONSTRUCT
#define _IWAVE_STATIC_CONSTRUCT

#ifdef __cplusplus
extern "C" {
#endif

int iwave_static_construct(double fs_in,
			 double tau_in,
			 double fline_in,
			 double fstate[]);
    
#ifdef __cplusplus
}
#endif

#endif /* #ifndef _IWAVE_STATIC_CONSTRUCT */
