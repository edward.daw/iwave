/* function declaration for a utility function to convert an */
/* arbitrary input frequency in radians per sample into a    */
/* frequency in the range 0 to pi radians per sample, corresponding */
/* to the positive frequency nyquist band */
/* Ed Daw, 1st March 2021 */
double iwave_firstbz(double deltain);