#include <math.h>
#include <iwave_definitions.h>
#include <iwave_pll_array_run.h>
#include <iwave_pll_run.h>
#include <iwave_state_data_structure.h>

int iwave_pll_array_run(int ndata, double indata[], double outdata[], 
        double ampdata[], double errordata[], 
        double freqdata[], double fstate[]) {
    int foundnan=0;
    int dcount,fcount,acount,nfilters;
    double* pstate; double* psubstate;
    statedata* ps; statedata* poths;
    double presubdata,pfilt,qfilt,fs,delta;
    double outd,outq,outa,oute,outf,outsub;
    
    /* get the number of filters from the state data */
    nfilters=(int)fstate[0];
    /* get the sampling frequency  */
    fs=fstate[1];    
    /* loop over data */
    for(dcount=0;dcount<ndata;++dcount) {
        /* trap NANs */
        if(isnan(indata[dcount])!=0) {
            indata[dcount]=0;
            foundnan=1;
        }
        /* store the current raw input sample */
        outsub=indata[dcount];
        /* loop over filters */
        for(fcount=0;fcount<nfilters;++fcount) {
           /* calculate start address of state data for current filter */
           pstate=fstate+(int)PLLARRAYDATASIZE+
               fcount*(int)PLLSTATESIZE;
           /* set state data structure to state data pointer for current filter */
           ps=(statedata*)pstate;
           /* set input to the raw input sample */
           presubdata=indata[dcount];
           /* presubtract predictors from all the other filters */
           for(acount=0;acount<nfilters;++acount) {
               /* only subtract OTHER filter outputs from this input */   
               if(acount != fcount) {
                   /* obtain pointer to subtracted filter data */
                   psubstate=fstate+(int)PLLARRAYDATASIZE+ 
                     acount*(int)PLLSTATESIZE; 
                   /* set aux state data structure to point for aux state */
                   /* data for filter currently being subtracted */
                   poths = (statedata*)psubstate;
                   /* get last outputs of filter to be subtracted */
                   pfilt=poths->ymemreal;
                   qfilt=poths->ymemimag;
                   /* get the current phase shift per sample */
                   delta=poths->delta;
                   /* time evolve forward one sample */
		   /* with sub, amplitude of 5e-3 p-p */
		   /* without sub (line below commented), amplitude 1e-2 */
		   /* so cross-subtraction definitely helps, but not much */
                   presubdata -= (pfilt*cos(delta) - qfilt*sin(delta));
               }
           }
           /* run this filter on the pre-subtracted data */
           iwave_pll_run(1,&presubdata,&outd,&outq,&outa,&oute,&outf,pstate);
           /* write the output frequency ata for this filter */
           freqdata[fcount*ndata+dcount]=outf;
           ampdata[fcount*ndata+dcount]=outa;
           errordata[fcount*ndata+dcount]=oute;
           /* subtract the filter output from the input data */
           outsub -= outd;
        }
        /* write the output data */
        outdata[dcount]=outsub;
    }
    if(foundnan == 0) {
      return 0;
    } else {
      return 1;
    }
}
