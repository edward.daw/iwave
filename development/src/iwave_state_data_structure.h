/* a structure mirror for the state data for IWAVE */
/* Ed Daw, 5th April 2019                          */
#ifndef _IWAVE_STATE_DATA_STRUCTURE_H
#define _IWAVE_STATE_DATA_STRUCTURE_H

#ifdef __cplusplus
extern "C" {
#endif
    
typedef struct statetype {
    double adiag;           /* [0] */
    double aoffdiag;        /* [1] */
    double b;               /* [2] */
    double r11;             /* [3] */
    double r12;             /* [4] */
    double r22;             /* [5] */
    double ymemreal;        /* [6] */
    double ymemimag;        /* [7] */
    double adiagusb;        /* [8] */
    double aoffdiagusb;     /* [9] */
    double busb;            /* [10] */
    double ymemrealusb;     /* [11] */
    double ymemimagusb;     /* [12] */
    double delta;           /* [13] */
    double gain;            /* [14] */
    double w;               /* [15] */
    double srate;           /* [16] */
} statedata;
    
#ifdef __cplusplus
}
#endif

#endif /* #ifndef _IWAVE_STATE_DATA_STRUCTURE_H */
