#include <math.h>
#include <iwave_definitions.h>
#include <iwave_pll_construct.h>
#include <iwave_state_data_structure.h>

int iwave_pll_construct(double fs_in,
			 double tau_in,
			 double fline_in,
			 double starttime,
			 double fstate[]) {

  /* gain set for maximally flat response - see labbook page 137 */
  double gain,delta,deltausb,w,absq;
  statedata* ps;
  
  /* set state data pointer to function argument */
  ps=(statedata*)fstate;
  /* calculate gain */
  gain=1/(2*tau_in*tau_in*fs_in*fs_in);
  /* initialise single-f static iwave */
  w=1/(fs_in*tau_in);
  delta=2*IWAVEPI*fline_in/fs_in;
  absq = (1-exp(-w))*(1-exp(-w))/(1-2*exp(-w)*cos(2*delta)+exp(-2*w));
  ps->adiag         = exp(-w)*cos(delta);  
  ps->aoffdiag      = exp(-w)*sin(delta);
  ps->b             = 1-exp(-w);
  ps->r11           = 2 - ps->b;
  ps->r12           = -ps->b * ps->adiag / ps->aoffdiag;
  ps->r22           = 4/(1-absq) - ps->r11;
  ps->ymemreal      = 0;
  ps->ymemimag      = 0;
  /* deltausb is the frequency where the upper sideband appears in */
  /* radians per sample */
  if(fline_in > (fs_in / 4)) {
      /* account for aliasing when line frequency exceeds half nyquist */
      deltausb=2*IWAVEPI*fabs(2*fline_in-fs_in)/fs_in;
  } else {
      /* no need to account for aliasing for line frequency less than */
      /* half nyquist */
      deltausb=2*delta;
  }
  ps->adiagusb      = exp(-2*w)*cos(deltausb);
  ps->aoffdiagusb   = exp(-2*w)*sin(deltausb);
  ps->busb          = 1-exp(-2*w);
  ps->ymemrealusb   = 0;
  ps->ymemimagusb   = 0;
  ps->delta         = delta;
  ps->gain          = gain;
  ps->w             = w;
  ps->srate         = fs_in; 
  return 0;
}