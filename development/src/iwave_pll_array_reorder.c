#include <iwave_definitions.h>
#include <iwave_pll_array_reorder.h>

int iwave_pll_array_reorder(int neworder[], double* fstatein, double* fstateout) {
    int nfilters;
    /* counter for single elements */
    int ecount;
    /* counter for filters */
    int fcount;
    double* pfilter;

    nfilters=(int)fstatein[0];
    /* copy header data */
    for(ecount=0;ecount<PLLARRAYDATASIZE;++ecount) {
        fstateout[ecount]=fstatein[ecount];
    }
    /* copy reorderered data to output array */
    for(fcount=0;fcount<nfilters;++fcount) {
        pfilter=fstatein+PLLARRAYDATASIZE+neworder[fcount];
        for(ecount=0;ecount<PLLSTATESIZE;++ecount) {
            fstateout[PLLARRAYDATASIZE+fcount*PLLSTATESIZE+ecount]=
              pfilter[ecount];
        }
    }

    return 0;
}