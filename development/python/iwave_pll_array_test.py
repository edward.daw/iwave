import numpy as np 
import iwave
import matplotlib.pyplot as plt 

# data set parameters
duration_secs = 30.0
srate_hz = 16384.0

# input data parameters
freq_wave1_hz = 60.0
freq_wave2_hz = 61.0

# iwave parameters
tau_secs = 1
freq_guess_hz = 10.0

# number of samples in data to be created
nsamps=int(duration_secs) * int(srate_hz)
# create time array
time = np.linspace(0,duration_secs-1/srate_hz,nsamps)
# create empty array for phase of sine wave
wave = np.zeros(nsamps,np.float64)

# populate wave
for count in range(nsamps):
    wave[count]= \
        np.sin(2 * iwave.pi() * freq_wave1_hz * time[count]) + \
        np.sin(2 * iwave.pi() * freq_wave2_hz * time[count])

# populate frequency initial guesses
fguess=np.empty(2,np.float64)
fguess[0]=50
fguess[1]=70

# set up double wave tracker
number_of_waves=2
statesize=iwave.array_header_size() + \
    number_of_waves * iwave.pll_size()
fstate=np.empty(statesize,np.float64)
# set up state data. Note that fmin and fmax are set
# to 0 and 8192 because these parameters currently 
# don't do anything.
iwave.pll_array_construct(srate_hz, tau_secs, \
    0, 8192, fguess, fstate)

# create the containers for the output data
outdata=np.empty(nsamps,np.float64)
amp=np.empty(nsamps*number_of_waves,np.float64)
error=np.empty(nsamps*number_of_waves,np.float64)
freq=np.empty(nsamps*number_of_waves,np.float64)

# run iwave array
iwave.pll_array_run(wave,outdata, \
    amp,error,freq,fstate)

# plot results
plt.figure(1)
plt.subplot(221)
plt.plot(time,outdata,'-k',linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('output data')
plt.subplot(222)
plt.plot(time,error[0:nsamps],'-k', \
    linewidth=0.5)
plt.plot(time,error[nsamps:2*nsamps],'-r', \
    linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('error')
plt.subplot(223)
plt.plot(time,freq[0:nsamps],'-k', \
    linewidth=0.5)
plt.plot(time,freq[nsamps:2*nsamps],'-r', \
    linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('frequency (Hz)')
plt.subplot(224)
plt.plot(time,amp[0:nsamps],'-k', \
    linewidth=0.5)
plt.plot(time,amp[nsamps:2*nsamps],'-r', \
    linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.show()
