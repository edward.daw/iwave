#! /usr/bin/env python

# System imports
from distutils.core import *
from distutils      import sysconfig

# NumPy supplies containers for the iwave state,
# as well as incoming and outgoing data samples.
import numpy

# Obtain the numpy include directory.  This logic works across numpy versions.
try:
    numpy_include = numpy.get_include()
except AttributeError:
    numpy_include = numpy.get_numpy_include()

# iwave extension module
_iwave = Extension('_iwave',
    extra_compile_args=['-I../src'],
    sources=['iwave.i',    
            '../src/iwave_definitions.c',
            '../src/iwave_static_construct.c',
            '../src/iwave_static_run.c',
            '../src/iwave_pll_construct.c',
            '../src/iwave_pll_run.c',
            '../src/iwave_pll_array_construct.c',
            '../src/iwave_pll_array_run.c',
            '../src/iwave_firstbz.c'],
    include_dirs = [numpy_include])

# iwave setup
setup(  name        = 'IWAVE python wrapper',
        description = 'Functions that wrap the IWAVE toolset into PYTHON using NUMPY arrays',
        author      = 'Ed Daw',
        version     = '3.1',
        ext_modules = [_iwave])
