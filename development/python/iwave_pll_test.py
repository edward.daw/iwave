import numpy as np 
import iwave
import matplotlib.pyplot as plt 

# data set parameters
duration_secs = 10.0
srate_hz = 16384.0

# input data parameters
freq_wave1_hz = 10.0
freq_wave2_hz = 11.0

# iwave parameters
tau_secs = 0.2
freq_guess_hz = 10.0
# start time locking delays not currently implemented
start_time_secs = 0.0

# phase shift per sample for the two waves
phase_shift_per_sample_wave1_rads = \
    2 * iwave.pi() * freq_wave1_hz / srate_hz
phase_shift_per_sample_wave2_rads = \
    2 * iwave.pi() * freq_wave2_hz / srate_hz

# number of samples in data to be created
nsamps=int(duration_secs) * int(srate_hz)
# create time array
time = np.linspace(0,duration_secs-1/srate_hz,nsamps)
# create empty array for phase of sine wave
wave = np.zeros(nsamps,np.float64)
# populate wave
phase = 0
for count in range(nsamps):
    if (count==0):
        phase=0
    elif (count<np.floor(nsamps/2)):
        phase += phase_shift_per_sample_wave1_rads
    else:
        phase += phase_shift_per_sample_wave2_rads
    wave[count]=np.sin(phase)

# initialise iwave
fstate = np.empty(iwave.pll_size(),np.float64)
iwave.pll_construct(srate_hz, tau_secs, freq_guess_hz, \
    start_time_secs, fstate)

# create output data arrays for iwave_pll
dout=np.empty(nsamps,np.float64)
qout=np.empty(nsamps,np.float64)
amp=np.empty(nsamps,np.float64)
error=np.empty(nsamps,np.float64)
freq=np.empty(nsamps,np.float64)

# run iwave
iwave.pll_run(wave,dout,qout,amp,error,freq,fstate)

# make plots
plt.figure(1)
plt.subplot(221)
plt.plot(time,amp,linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.subplot(222)
plt.plot(time,error,linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('error')
plt.subplot(223)
plt.plot(time,freq,linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('frequency (Hz)')
plt.subplot(224)
plt.plot(time,dout,linewidth=0.5)
plt.plot(time,qout,linewidth=0.5)
plt.xlabel('time (s)')
plt.ylabel('D out')
plt.show()
