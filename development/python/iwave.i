%module iwave

%{ /* Portions of this code enclosed in percents 
and curly braces are read as-is by the C compiler.
Since the C code in this SWIG source file includes
calls to C functions, the headers that include declarations of these functions are included below
using the usual C syntax. */ %}

%{
    #define SWIG_FILE_WITH_INIT
    #include "../src/iwave_definitions.h"
    #include "../src/iwave_static_construct.h"
    #include "../src/iwave_static_run.h"
    #include "../src/iwave_pll_construct.h"
    #include "../src/iwave_pll_run.h"
    #include "../src/iwave_pll_array_construct.h"
    #include "../src/iwave_pll_array_run.h"
    #include "../src/iwave_firstbz.h"
%}

%# Portions of this code starting with a percent but not in curly braces are interpreted using python, so, for example, this python style comment where starting with a hash is ignored by the PYTHON interpreter.

%include "numpy.i"

%init %{
    import_array();
%}

%include "../src/iwave_definitions.h"

%rename (pi) my_iwave_pi;

%inline %{
    double my_iwave_pi(void) {
        return iwave_pi();
    }
%}

%rename (static_size) my_iwave_static_size;

%inline%{
    int my_iwave_static_size(void) {
        return iwave_static_size();
    }
%}

%rename (pll_size) my_iwave_pll_size;

%inline%{
    int my_iwave_pll_size(void) {
        return iwave_pll_size();
    }
%}

%rename (array_header_size) my_iwave_array_header_size;

%inline%{
    int my_iwave_array_header_size(void) {
        return iwave_array_header_size();
    }
%}

%rename (firstbz) my_iwave_firstbz;

%inline%{
    double my_iwave_firstbz(double deltain) {
        return iwave_firstbz(deltain);
    }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {(int len3, double* vec3)}

%include "../src/iwave_static_construct.h"

%rename (static_construct) my_iwave_static_construct;

%inline%{
    int my_iwave_static_construct(double fs_in,
        double tau_in, double fline_in, int len3, 
        double* vec3) {
            return iwave_static_construct(fs_in, 
                tau_in, fline_in, vec3);
    }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {(int len4, double* vec4), (int len5, double* vec5), (int len6, double* vec6),
(int len7, double* vec7)}

%include "../src/iwave_static_run.h"

%rename (static_run) my_iwave_static_run;

%inline%{
    int my_iwave_static_run(int len4, double* vec4,
                            int len5, double* vec5,
                            int len6, double* vec6,
                            int len7, double* vec7) {
        return iwave_static_run(len4, vec4, vec5, vec6, vec7);
    }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {(int len8, double* vec8)}

%include "../src/iwave_pll_construct.h"

%rename (pll_construct) my_iwave_pll_construct;

%inline%{
    int my_iwave_pll_construct(double fs_in,
                            double tau_in,
                            double fline_in,
                            double starttime,
                            int len8, double* vec8) {
        return iwave_pll_construct(fs_in, tau_in, fline_in,
            starttime, vec8);
    }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {
    (int len9, double* vec9), 
    (int len10, double* vec10), 
    (int len11, double* vec11), 
    (int len12, double* vec12), 
    (int len13, double* vec13), 
    (int len14, double* vec14), 
    (int len15, double* vec15)}

%include "../src/iwave_pll_run.h";

%rename (pll_run) my_iwave_pll_run;

%inline%{
    int my_iwave_pll_run(int len9, double* vec9, 
                        int len10, double* vec10,
                        int len11, double* vec11, 
                        int len12, double* vec12, 
                        int len13, double* vec13, 
                        int len14, double* vec14, 
                        int len15, double* vec15)  {
        return iwave_pll_run(len9, vec9, vec10, vec11, vec12, vec13, vec14, vec15);
   }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {
    (int len16, double* vec16),
    (int len17, double* vec17)}

%include "../src/iwave_pll_array_construct.h";

%rename (pll_array_construct) my_iwave_pll_array_construct;

%inline%{
    int my_iwave_pll_array_construct(
        double fs,
        double tau,
        double fmin,
        double fmax,
        int len16, double* vec16,
        int len17, double* vec17) {
            return iwave_pll_array_construct(
                      len16, fs, tau, fmin, fmax,  vec16, vec17);
    }
%}

%apply (int DIM1, double* INPLACE_ARRAY1) {
    (int len18, double* vec18),
    (int len19, double* vec19),
    (int len20, double* vec20),
    (int len21, double* vec21),
    (int len22, double* vec22),
    (int len23, double* vec23)}

%include "../src/iwave_pll_array_run.h";

%rename (pll_array_run) my_iwave_pll_array_run;

%inline%{
    int my_iwave_pll_array_run(
        int len18, double* vec18, 
        int len19, double* vec19,
        int len20, double* vec20,
        int len21, double* vec21,
        int len22, double* vec22, 
        int len23, double* vec23
    )  {
            return iwave_pll_array_run( len18,
               vec18, vec19, vec20, vec21, vec22, vec23);   
    }   
%}


