import numpy as np
import iwave
import matplotlib.pyplot as plt

# create an array of time samples
duration_secs=5
samp_rate=256
samp_period=1/samp_rate
time=np.linspace(0,duration_secs-samp_period,duration_secs*samp_rate)

# generate a sine wave at some frequency
sine_freq_hz=3
sinewave=np.empty(len(time),np.float64)
for count in range(len(time)):
    sinewave[count]=np.sin(2*iwave.pi()*sine_freq_hz*time[count])

# generate state vector for static iwave
fstate=np.empty(iwave.static_size(),np.float64)
tau_secs=1.0
iwave.static_construct(samp_rate,tau_secs,sine_freq_hz,fstate)

# dimension arrays for output
dout=np.empty(len(time),np.float64)
qout=np.empty(len(time),np.float64)

# run iwave static line tracker
iwave.static_run(sinewave,dout,qout,fstate)

# compute amplitude of wave
ampout=np.empty(len(time),np.float64)
for count in range(len(time)):
    ampout[count]=np.sqrt(dout[count]*dout[count]+qout[count]*qout[count])

# generate plot
inputline, =plt.plot(time,sinewave,'r',linewidth=0.5,label='input data')
dline, =plt.plot(time,dout,'k',linewidth=0.5,label='in phase output')
qline, =plt.plot(time,qout,'b',linewidth=0.5,label='quad phase output')
ampline, =plt.plot(time,ampout,'g',linewidth=0.5, label='amplitude output')
plt.legend(handles=[inputline,dline,qline,ampline], loc='lower right', framealpha=1)
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.grid(color='k', linestyle=":", linewidth=0.5)
plt.tick_params(direction='in')
plt.show()

