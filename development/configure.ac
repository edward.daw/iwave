#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.63])
AC_INIT([iwave], [3.2], [e.daw@sheffield.ac.uk])
m4_include([m4/matlab.m4])
m4_pattern_allow([AM_PROG_AR])
AM_PROG_AR
AM_INIT_AUTOMAKE([-Wall -Werror foreign])
AC_PROG_CC
AC_CHECK_LIB(m, cos)
AC_CONFIG_MACRO_DIR([m4])
LT_INIT
AC_CONFIG_HEADERS([config.h])
# check to see if the configure script was invoked with --with-matlab
# and also check that matlab, mex, and mexext are present
CHECK_MATLAB
AM_CONDITIONAL([COND_MATLAB_MEX], [test "$havemex" = true])
CHECK_MEXEXT
AM_CONDITIONAL([COND_MATLAB_MEXEXT], [test "$havemexext" = true])
# check to see if the configure script was invoked with --with-python
# and also check that python and swig can be found
CHECK_PYTHONSWIG
AM_CONDITIONAL([COND_PYTHON_SWIG], [test "$pythonswig" = true])
if test "$pythonswig" = "true"
then
    AC_PROG_SWIG
    SWIG_PYTHON
else
# the mere presence of AC_PROG_SWIG and SWIG_PYTHON, even inside a conditional causes
# python verbage to occur in the configure script. These variables must be set 
# otherwise the configure script quits with an error and never builds anything. It's
# a (very common) gnu autotools bug that most developers have lost interest in
# addressing as the gnu autotools have gone out of style.
    BUILD_PYTHON_SWIG_TRUE='#'
    BUILD_PYTHON_SWIG_FALSE=
fi

if test "$have_matlab" = "no"
then
    AC_MSG_ERROR([Must specify matlab directory using --with-matlab])
fi
AC_MSG_NOTICE([Generating makefiles])
AC_CONFIG_FILES([Makefile src/Makefile src/libiwave.pc matlab/Makefile python/Makefile ])
AC_MSG_NOTICE([Doing package configure])
AC_SUBST([pkgconfigdir], [${libdir}/pkgconfig])
AC_OUTPUT
