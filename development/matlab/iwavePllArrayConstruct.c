#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_pll_array_construct.h"
#include "../src/iwave_pll_construct.h"

void mexFunction( int nlhs,
          mxArray *plhs[],
          int nrhs,
          const mxArray *prhs[]) {

int nfilters;
double fs,tau,fmin,fmax;
double* fstate; double* farray;

/* read input data */ 
nfilters=(int)(*((double*)mxGetPr(prhs[0])));
fs=(*((double*)mxGetPr(prhs[1])));
tau=(*((double*)mxGetPr(prhs[2])));
fmin=(*((double*)mxGetPr(prhs[3])));
fmax=(*((double*)mxGetPr(prhs[4])));
farray=(double*)mxGetPr(prhs[5]);

/* allocate for the output data */
plhs[0]=mxCreateDoubleMatrix(1,(int)PLLARRAYDATASIZE + 
        nfilters*(int)PLLSTATESIZE,mxREAL);
fstate=(double*)mxGetPr(plhs[0]);
          
/* call IWAVE array constructor here */          
iwave_pll_array_construct(nfilters, fs, tau, fmin, fmax, farray, fstate);
                    
return;
}

#include "../src/iwave_pll_array_construct.c"
#include "../src/iwave_pll_construct.c"
