function iwave_pll_array_test
% IWAVE_PLL_ARRAY_TEST - example of application of the iwave pll array
% test tool - functionality mirrors that of iwave_pll_array_test.py in
% the python subdirectory.
%
% useage: iwave_pll_array_test;
%
% Ed Daw, e.daw@sheffield.ac.uk, 26th May 2020

% data set parameters
duration_secs=30.0;
srate_hz=16384.0;

% input data parameters
freq_wave1_hz = 60.0;
freq_wave2_hz = 61.0;

% iwave parameters
tau_secs=1;
freq_guess_hz=[50.0 70.0];

% number of samples in data to be created
nsamps=floor(duration_secs*srate_hz);
% create time array
time=linspace(0,duration_secs-1/srate_hz,nsamps);
% create wave
wave=sin(2*pi*freq_wave1_hz*time)+sin(2*pi*freq_wave2_hz*time);

% initialise iwave - note that the upper and lower frequency limits
% set to DC and the Nyquist frequency here currently don't do anything.
number_of_waves=2;
fstate=iwavePllArrayConstruct(number_of_waves, srate_hz, tau_secs, ...
    0, srate_hz/2, freq_guess_hz);
format long;

% run iwave array - note the argument order differs from that in the 
% python wrapper here. 
[outdata, freq, amp, error, fstate]= ...
    iwavePllArrayRun(wave,fstate);

% plot results
figure;
subplot(2,2,1);
plot(time,outdata,'-k');
xlabel('time (s)');
ylabel('output data');
subplot(2,2,2);
plot(time,error(1:nsamps),'-k', ...
    time,error(nsamps+1:2*nsamps),'-r');
xlabel('time (s)');
ylabel('error');
subplot(2,2,3);
plot(time,freq(1:nsamps),'-k', ...
    time,freq(nsamps+1:2*nsamps),'-r');
xlabel('time (s)');
ylabel('frequency');
subplot(2,2,4);
plot(time,amp(1:nsamps),'-k', ...
    time,amp(nsamps+1:2*nsamps),'-r');
xlabel('time (s)');
ylabel('amplitude');
