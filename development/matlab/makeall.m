function makeall
% MAKEALL - rebuild all mex files in the iwave distributionn
% 
% useage: makeall
%
% Ed Daw, e.daw@sheffield.ac.uk, 1st February 2019

disp('Making iwavePllArrayConstruct.c');
mex -I../src iwavePllArrayConstruct.c
disp('Making iwavePllArrayRun.c');
mex -I../src iwavePllArrayRun.c
disp('Making iwavePllConstruct.c');
mex -I../src iwavePllConstruct.c
disp('Making iwavePllRun.c');
mex -I../src iwavePllRun.c
disp('Making iwaveStaticConstruct.c');
mex -I../src iwaveStaticConstruct.c
disp('Making iwaveStaticRun.c');
mex -I../src iwaveStaticRun.c
