% IWAVESTATICCONSTRUCT - construct an iwave phase locked loop filter
% for use with real input data, where the frequency of the input wave
% is static.
% 
% useage: <state data vector> = ...
%                iwaveStaticConstruct( <sampling rate (Hz)>, ...
%                                   <response time (s)>, ...
%                                   <line frequency guess (Hz)>)
%
% Ed Daw, e.daw@sheffield.ac.uk, 29th January 2019

