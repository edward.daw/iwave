#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_static_construct.h"

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {
    
    double* psrate; double* ptau; double* pfline; double* pfstate;
    /* extract input variables */
    psrate=(double*)mxGetPr(prhs[0]);
    ptau=(double*)mxGetPr(prhs[1]);
    pfline=(double*)mxGetPr(prhs[2]);
    /* dimension output array */
    plhs[0]=mxCreateDoubleMatrix(1,STATESIZE,mxREAL);
    pfstate=mxGetPr(plhs[0]);
    /* call initialisation routine from iwave library */
    iwave_static_construct(*psrate, *ptau, *pfline, pfstate);
    
    return;
}

#include "../src/iwave_static_construct.c"