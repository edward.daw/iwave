% IWAVEPLLRUN - run a phase locked loop IWAVE filter (under construction)
%
% useage: [<in phase output>, <q phase output>, ...
%          <amplitude output>, <error output>, ...
%          <frequency output> , <state output>] = ...
%            iwavePllRun(<input data>, <state input>);
%
% where <state_input> is a vector of state data previously created using
% iwaveStaticConstruct, or fed from the <state output> of a previous iteration
% of iwaveStaticRun on a previous contiguous portion of data.
%
% Ed Daw, 29th January 2019
