function iwave_pll_test
% IWAVE_PLL_TEST - demonstration of the use of the iwave phaselocked line
% tracker via the matlab wrapper
%
% useage: iwave_pll_test;
%
% Ed Daw, e.daw@sheffield.ac.uk, 26th May 2020

% data set parameters
duration_secs=10.0;
srate_hz=16384.0;

% input data parameters
freq_wave1_hz=10.0;
freq_wave2_hz=11.0;

% iwave paramters
tau_secs=0.2;
freq_guess_hz=10.0;
% start time locking delays not currently implemented
start_time_secs=0.0;

% phase shift per sample for the two waves
phase_shift_per_sample_wave1_rads = ...
    2*pi*freq_wave1_hz/srate_hz;
phase_shift_per_sample_wave2_rads = ...
    2*pi*freq_wave2_hz/srate_hz;

% number of samples in data to be created
nsamps = floor(duration_secs*srate_hz);
time=linspace(0,duration_secs-1/srate_hz, nsamps);
wave=zeros(1,nsamps);
% populate wave with a sinusoid whose frequency changes half way through
for count=1:nsamps
    if count==1
        phase=0;
    elseif(count<floor(nsamps/2))
        phase=phase+phase_shift_per_sample_wave1_rads;
    else
        phase=phase+phase_shift_per_sample_wave2_rads;
    end
    wave(count)=sin(phase);
end

% initialise iwave pll
fstate=iwavePllConstruct(srate_hz,tau_secs,freq_guess_hz, ...
    start_time_secs);

% run iwave pll
[dout,qout,amp,error,freq,fstate]=iwavePllRun(wave,fstate);

% plot results
figure;
subplot(2,2,1);
plot(time,amp,'-');
xlabel('time (s)');
ylabel('amplitude');
subplot(2,2,2);
plot(time,error,'-');
xlabel('time (s)');
ylabel('error');
subplot(2,2,3);
plot(time,freq,'-');
xlabel('time (s)');
ylabel('frequency (Hz)');
subplot(2,2,4);
plot(time,dout,'-',time,qout,'-');
xlabel('time (s)');
ylabel('D and Q out');


