% IWAVESTATICRUN - run a static frequency IWAVE filter
%
% useage: [<in phase output>, <q phase output>, <state output>] = ...
%            iwaveStaticRun(<input data>,<state input>);
%
% where <state_input> is a vector of state data previously created using
% iwaveStaticConstruct, or fed from the <state output> of a previous iteration
% of iwaveStaticRun on a previous contiguous portion of data.
%
% Ed Daw, e.daw@sheffield.ac.uk, 29th January 2019
