#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_firstbz.h"
#include "../src/iwave_pll_run.h"

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {

    double* pindata; double* pfstate; double* pfout;
    double* pdout; double* pqout; double* pfstateout;
    double* paout; double* peout;
    int datalength,scount,foundnan;

    /* get input data */
    pindata=(double*)mxGetPr(prhs[0]);
    datalength=(int)mxGetM(prhs[0])*(int)mxGetN(prhs[0]);
    pfstate=(double*)mxGetPr(prhs[1]);
    
    /* allocate memory for output data */
    plhs[0]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    pdout=(double*)mxGetPr(plhs[0]);
    plhs[1]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    pqout=(double*)mxGetPr(plhs[1]);
    plhs[2]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    paout=(double*)mxGetPr(plhs[2]);
    plhs[3]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    peout=(double*)mxGetPr(plhs[3]);
    plhs[4]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    pfout=(double*)mxGetPr(plhs[4]);
    plhs[5]=mxCreateDoubleMatrix(1,PLLSTATESIZE,mxREAL);
    pfstateout=(double*)mxGetPr(plhs[5]);
   
    /* copy elements of state vector to output state vector */
    for(scount=0;scount<PLLSTATESIZE;++scount) {
        pfstateout[scount]=pfstate[scount];
    }
    
    /* run the filter */    
    foundnan=iwave_pll_run(datalength, pindata, pdout, pqout,
            paout, peout, pfout, pfstateout);
    if(foundnan == 1) {
        mexWarnMsgIdAndTxt("iwave_pll_run()","NANs detected in input data");
    }
    return;
}

#include "../src/iwave_pll_run.c"
#include "../src/iwave_firstbz.c"