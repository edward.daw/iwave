% IWAVEPLLARRAYCONSTRUCT - construct the state data for an array of iwave
% filters.
%
% useage: <state data array> = iwavePllArrayConstruct( ...
%                                     <number of filters>,
%                                     <sampling rate (Hz)>,
%                                     <filter response time (s)>,
%                                     <lower frequency limit (Hz)>,
%                                     <upper frequency limit (Hz)>,
%                                     <array of frequency guesses (Hz)>)
%
% Ed Daw, e.daw@sheffield.ac.uk, 31st January 2019


