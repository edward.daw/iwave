#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_firstbz.h"
#include "../src/iwave_pll_run.h"
#include "../src/iwave_pll_array_run.h"

void mexFunction( int nlhs,
          mxArray *plhs[],
          int nrhs,
          const mxArray *prhs[]) {

    
    int datalength, nfilters, statesize, scount, foundnan=0;
    double* indata; double* outdata; double* fdata; double* state;
    double* ampdata; double* errordata;
    double* outstate;

    /* get data input */
    indata=(double*)mxGetPr(prhs[0]);
    /* measure the length of input data to be processed */
    datalength=(int)mxGetM(prhs[0])*(int)mxGetN(prhs[0]);
    /* get a pointer to the input state data */
    state=(double*)mxGetPr(prhs[1]);
    /* get number of filters */
    nfilters=(int)state[0];

    statesize=(int)PLLARRAYDATASIZE + nfilters*(int)PLLSTATESIZE;

    /* allocate memory to output data */
    plhs[0]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    outdata=(double*)mxGetPr(plhs[0]);
    plhs[1]=mxCreateDoubleMatrix(1,datalength*nfilters,mxREAL);
    fdata=(double*)mxGetPr(plhs[1]);
    plhs[2]=mxCreateDoubleMatrix(1,datalength*nfilters,mxREAL);
    ampdata=(double*)mxGetPr(plhs[2]);
    plhs[3]=mxCreateDoubleMatrix(1,datalength*nfilters,mxREAL);
    errordata=(double*)mxGetPr(plhs[3]);
    plhs[4]=mxCreateDoubleMatrix(1,statesize,mxREAL);
    outstate=(double*)mxGetPr(plhs[4]);

    /* copy state data to outstate data */
    for(scount=0;scount<statesize;++scount) {
        outstate[scount]=state[scount];
    }

    foundnan=iwave_pll_array_run(datalength, indata, outdata, 
            ampdata, errordata, fdata, outstate);
    
    /* send warning if NANs were found in the input data */
    if(foundnan == 1) {
        mexWarnMsgIdAndTxt("iwave_pll_run()","NANs detected in input data");
    }
    return;
}

#include "../src/iwave_pll_array_run.c"
#include "../src/iwave_pll_run.c"
#include "../src/iwave_firstbz.c"