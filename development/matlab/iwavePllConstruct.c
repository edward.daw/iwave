#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_pll_construct.h"

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {
    
    double* psrate; double* ptau; double* pfline;
    double* pstarttime; double* pfstate;
    /* extract input variables */
    psrate=(double*)mxGetPr(prhs[0]);
    ptau=(double*)mxGetPr(prhs[1]);
    pfline=(double*)mxGetPr(prhs[2]);
    pstarttime=(double*)mxGetPr(prhs[3]);
    /* dimension output array */
    plhs[0]=mxCreateDoubleMatrix(1,PLLSTATESIZE,mxREAL);
    pfstate=(double*)mxGetPr(plhs[0]);
    /* call initialisation routine from iwave library */
    iwave_pll_construct(*psrate, *ptau, *pfline, *pstarttime, pfstate);
    
    return;
}

/* function definition is also in a header so that the source code */
/* can be shared with a matlab-independent IWAVE library.          */

#include "../src/iwave_pll_construct.c"