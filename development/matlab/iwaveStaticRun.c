#include <mex.h>
#include <math.h>
#include "../src/iwave_definitions.h"
#include "../src/iwave_static_run.h"

void mexFunction( int nlhs,
		  mxArray *plhs[], 
		  int nrhs, 
		  const mxArray *prhs[]) {

    double* pindata;
    double* pfstate;
    double* pdout; double* pqout; double* pfstateout;
    int datalength,scount,foundnan;
    
    /* get input data */
    pindata=(double*)mxGetPr(prhs[0]);
    datalength=(int)mxGetM(prhs[0])*(int)mxGetN(prhs[0]);
    pfstate=(double*)mxGetPr(prhs[1]);
    
    /* allocate memory for output data */
    plhs[0]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    pdout=(double*)mxGetPr(plhs[0]);
    plhs[1]=mxCreateDoubleMatrix(1,datalength,mxREAL);
    pqout=(double*)mxGetPr(plhs[1]);
    plhs[2]=mxCreateDoubleMatrix(1,STATESIZE,mxREAL);
    pfstateout=(double*)mxGetPr(plhs[2]);
    
    for(scount=0;scount<(int)STATESIZE;++scount) {
        pfstateout[scount]=pfstate[scount];
    }

    foundnan=iwave_static_run(datalength, pindata, pdout, pqout, pfstateout);
    if(foundnan == 1) {
        mexWarnMsgIdAndTxt("iwave_static_run()","NANs detected in input data");
    }
    return;
}

#include "../src/iwave_static_run.c"