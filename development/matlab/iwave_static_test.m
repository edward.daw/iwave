function iwave_static_test
% IWAVE_STATIC_TEST - demonstration of use of the iwave static line
% tracker via the matlab wrapper
%
% useage: iwave_static_test;
%
% Ed Daw, e.daw@sheffield.ac.uk, 26th May 2020

duration_secs=5;
samp_rate=256;
samp_period=1/samp_rate;

% generate an array of times
time=linspace(0,duration_secs-samp_period, duration_secs*samp_rate);

% generate a sine wave at some frequency
sine_freq_hz=3;
sinewave=sin(2*pi*sine_freq_hz*time);

% generate a state vector for static iwave
tau_secs=1.0;
fstate=iwaveStaticConstruct(samp_rate, tau_secs, sine_freq_hz);

% run iwave
[dout,qout,fstate]=iwaveStaticRun(sinewave,fstate);

% display results
amp=sqrt(dout.^2+qout.^2);
plot(time,sinewave,'-',time,dout,'-',time,qout,'-',time,amp,'-');
xlabel('time (s)');
ylabel('amplitude');
grid on;


