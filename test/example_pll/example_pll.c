#include <stdio.h>
#include <iwave.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

/* DEFINEs for things you don't need to change often - hardwire them */
/* Ensure that DATASIZE is equal to the product of the total time and the sampling rate */
#define DATASIZE 5632

float gasdev(long *idum);

/* function declaration for chisquare */
double chisquare(double fin[], double fout[], int nsamples);

int main(int argc, char* argv[]) {

/* default values used for variables that can optionally be set using command line arguments */

    int nargs;
    double samprate=256.0;
    double freqguess=20.0;
    double tau;
    double taumin = 0.01; /* starting value of tau for testing */
    double taumax = 5.0; /* upper limit of tau */
    double starttime=0.0;
    int ac;
    int taupoints = 500; /* number of times tau is to be changed between taumin and taumax */
    int tc; /* counter for tau */
    char** arg;
    double pdata[DATASIZE];
    double time[DATASIZE];
    double dout[DATASIZE];
    double qout[DATASIZE];
    double aout[DATASIZE];
    double eout[DATASIZE];
    double fout[DATASIZE];
    double fin[DATASIZE]; /* Input frequency to iwave */
    int dc;
    double amplitude = 1.0;
    double snr = 2.0; /* ratio of the amplitude of the signal to that of the added noise */
    double phaseaccum; /* cumulative phase */
    double freqchange=0.1; /* rate of change of frequency df/dt in Hz per second. */
    /* Do not alter the seed idum once set to some arbitrary negative integer value. */
    /* (If not set, Default value of idum is -1.)It should stay the same for as long */
    /* as the generator is used at any one sitting. It is an iterative generator, */
    /* so repeated use with the same value of idum always generates the same sequence */
    /* of random numbers. */
    long idum = -128503;
    char filename[2000];
    char filename2[2000];
    char filename3[2000];
    FILE* outfile; FILE* outfile2; FILE* outfile3;
    FILE* fp; FILE* fp2; FILE* fp3;
    /* storage for iwave state data. note that STATESIZE is defined in iwave_definitions.h, which */
    /* is included by iwave.h, which is a hold-it-all include file for the whole iwave distribution */
    double fstate[PLLSTATESIZE];
    double chisquared; /* sum of squares of differences betwen observed and expected frequencies */
    int writefile=0;
    int makeplot=0;

/* This is code to read the command line arguments. */
/* Notice it is written so that several arguments can come in with order*/
/* as executed. Also notice there are defaults above for each of them */
    ac=1;
    arg=argv+1;
    while(ac<argc) {
        if(strcmp(*arg,"-f")==0) {
            freqguess=atof(*(arg+1));
            printf("Frequency %.2lf Hz.\n",freqguess);
        } else if (strcmp(*arg,"-sr")==0) {
            samprate=atof(*(arg+1));
            printf("Sampling rate of %.2lf Hz.\n",samprate);
        } else if (strcmp(*arg,"-tresp")==0) {
            tau=atof(*(arg+1));
            printf("Response time of %.6lf seconds.\n",tau);
        } else if (strcmp(*arg,"-fch")==0) {
            freqchange=atof(*(arg+1));
            printf("Frequency change of %.2lf Hz per second.\n",freqchange);
        } else if (strcmp(*arg,"-snr")==0) {
            snr=atof(*(arg+1));
            printf("Signal to noise ratio of %.2lf.\n",snr);
        } else if (strcmp(*arg,"-writefile")==0) {
            writefile=1;
            printf("Write results to labelled file for plotting with Excel and Mathcad.\n");   
        } else if (strcmp(*arg, "-makeplot")==0) {
            makeplot=1;
            printf("Output data file and make screen gnuplots of results.\n");
        } else {
            printf("Syntax error. Useage: %s -f <frequency> -sr <sampling rate> -tresp <response time> -fch <frequency change> -snr <signal to noise ratio> -writefile (to write results to labelled file) -makeplot (to generate plots).\n",*argv);
        }
        ac+=2;
        arg+=2;
    }
    
    /* Open file for output of automated varying tau */
    sprintf(filename3, "results_changing_tau_snr%.2lf_freqchange%.2lf.txt",snr,freqchange);
    outfile3=fopen(filename3,"w");
    fprintf(outfile3,"# Output of IWAVE with varying frequency and additive white Gaussian noise for a range of values of tau.\n");
    fprintf(outfile3,"# Frequency %.2lf Hz, Sampling rate: %.2lf Hz\n",freqguess, samprate);
    fprintf(outfile3,"# Frequency change: %.2lf Hz per second, Signal to noise ratio: %.2lf\n",freqchange, snr);
    fprintf(outfile3,"# Columns:\n");
    fprintf(outfile3,"#|tau (s)|chi squared|\n");

    /* generate taupoints number of samples with varying tau that are linear in logarithmic space */
       for(tc=0;tc<taupoints;++tc){
            tau = pow(10,log10(taumin)+((log10(taumax)-log10(taumin))*tc/taupoints));

    double firstperiod=2.0; /* first period, in which frequency is static, in seconds */
    double modperiod1=20.0; /* period during which frequency is changing and noise is added in seconds */
    double modperiod2=0.0; /* period during which frequency is changing without noise in seconds */

    phaseaccum = 0; /* set phasecum to zero before first for loop */
    /* generate the wave data for first static third of array. */
    for(dc=0;dc<(int)(firstperiod*samprate);++dc) {
        /* demonstrate array-like syntax for pointing to addresses */
        time[dc]=dc/samprate;
        phaseaccum = phaseaccum + 2*IWAVEPI*freqguess*(1/samprate); /* integrate cumulative phase */
        /* demonstrate pointer arithmetic syntax for pointing to addresses */
        *(pdata+dc)=amplitude*sin(phaseaccum); /* generate waveform data input to IWAVE */
        *(fin+dc) = freqguess; /* expected frequency */
    }

    /* generate the wave data for second section of array where frequency changes and Gaussian noise is added */
    for(dc=(int)(firstperiod*samprate);dc<(int)((firstperiod+modperiod1)*samprate);++dc) {
        /* demonstrate array-like syntax for pointing to addresses */
        time[dc]=dc/samprate;
        phaseaccum = phaseaccum + 2*IWAVEPI*(freqguess+freqchange*(dc-(firstperiod*samprate))/samprate)*1/samprate; /* integrate cumulative phase change */
        /* add random Gaussian noise to signal using gasdev(&seed) */
        /* demonstrate pointer arithmetic syntax for pointing to addresses */
        *(pdata+dc)=amplitude*((gasdev(&idum)/snr)+sin(phaseaccum)); /* generate waveform data input to IWAVE */
        *(fin+dc) = (freqguess+freqchange*(dc-(firstperiod*samprate))/samprate); /* calculate expected frequency */
    }

    /* generate the wave data for third section of array where frequency changes but no Gaussian noise is added */
    for(dc=(int)((firstperiod+modperiod1)*samprate);dc<(int)((firstperiod+modperiod1+modperiod2)*samprate);++dc) {
        /* demonstrate array-like syntax for pointing to addresses */
        time[dc]=dc/samprate;
        phaseaccum = phaseaccum + 2*IWAVEPI*(freqguess+freqchange*(dc-(firstperiod*samprate))/samprate)*1/samprate; /* integrate cumulative phase change */
        /* add random Gaussian noise to signal using gasdev(&seed) */
        /* demonstrate pointer arithmetic syntax for pointing to addresses */
        *(pdata+dc)=amplitude*((gasdev(&idum)/snr)+sin(phaseaccum)); /* generate waveform data input to IWAVE */
    /*    *(pdata+dc)=amplitude*sin(phaseaccum); /* without noise */
        *(fin+dc) = (freqguess+freqchange*(dc-(firstperiod*samprate))/samprate); /* calculate expected frequency */
    }

    /* generate the wave data for last static frequency with Gaussian noise part of array */
    for(dc=((firstperiod+modperiod1+modperiod2)*samprate);dc<(int)DATASIZE;++dc) {
    /* demonstrate array-like syntax for pointing to addresses */
    time[dc]=dc/samprate;
    phaseaccum = phaseaccum + 2*IWAVEPI*(freqguess+(freqchange*(modperiod1+modperiod2)))*(1/samprate); /* integrate cumulative phase */
       /* add random Gaussian noise to signal using gasdev(&seed) */
       /* demonstrate pointer arithmetic syntax for pointing to addresses */
        *(pdata+dc)=amplitude*((gasdev(&idum)/snr)+sin(phaseaccum)); /* generate waveform data input to IWAVE */
       /*   *(pdata+dc)=amplitude*sin(phaseaccum); /* without noise */
        *(fin+dc) = (freqguess+(freqchange*(modperiod1+modperiod2))); /* calculate expected frequency */
    }

    iwave_pll_construct(samprate,tau,freqguess,starttime,fstate);

/*  run iwave on data */
    iwave_pll_run((int)DATASIZE,pdata,dout,qout,aout,eout,fout,fstate);

/*  measure chi squared */
    chisquared = chisquare(fin, fout, (int)DATASIZE);

/*  Print chi squared score to screen, only for individual values of tau */
/*    printf("Chi squared score of %.2lf\n",chisquared);*/

/*  output results of changing tau to a file */
    fprintf(outfile3,"%.6lf\t%.2lf\n",tau,chisquared);
}
    fclose(outfile3);

/*  write results to labelled file that can be plotted in Matlab and Excel */
    if(writefile==1) {  
        sprintf(filename, "results_samprate%.2lf_tau%.6lf_freqchange%.2lf_snr%.2lf.txt",samprate,tau,freqchange,snr);
        outfile=fopen(filename,"w");
        fprintf(outfile,"# A plot of the output of IWAVE with varying frequency and additive white Gaussian noise.\n");
        fprintf(outfile,"# Frequency %.2lf Hz, Sampling rate: %.2lf Hz, Response time: %.6lf seconds \n",freqguess, samprate, tau);
        fprintf(outfile,"# Frequency change: %.2lf Hz per second, Signal to noise ratio: %.2lf, Chi squared score of %.2lf\n",freqchange, snr,chisquared);
        fprintf(outfile,"# Columns:\n");
        fprintf(outfile,"#|Time (s)  |      Data   |    D output  |    Q output  |    Amplitude  |       Error  |Output freq (Hz)| |Input freq (Hz)|\n");
        for(dc=0;dc<DATASIZE;++dc) {
            fprintf(outfile,"%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\n",time[dc],pdata[dc],dout[dc],qout[dc],
            aout[dc],eout[dc],fout[dc],fin[dc]);
        }
        fclose(outfile);
    }

/* write results to text file to make plots with gnuplot if invoked on command line. NB the output file, results.txt, will be overwritten each time the program is run */
    if(makeplot==1) {    
        sprintf(filename2, "results.txt");
        outfile2=fopen(filename2,"w");
        fprintf(outfile2,"# A plot of the output of IWAVE with varying frequency and additive white Gaussian noise.\n");
        fprintf(outfile2,"# Frequency %.2lf Hz, Sampling rate: %.2lf Hz, Response time: %.6lf seconds, \n",freqguess, samprate, tau);
        fprintf(outfile2,"# Frequency change: %.2lf Hz per second, Signal to noise ratio: %.2lf\n",freqchange, snr);
        fprintf(outfile2,"# Columns:\n");
        fprintf(outfile2,"#|Time (s)  |      Data   |    D output  |    Q output  |    Amplitude  |       Error  |Output freq (Hz)| Input freq (Hz)|\n");
        for(dc=0;dc<DATASIZE;++dc) {
            fprintf(outfile2,"%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\n",time[dc],pdata[dc],dout[dc],qout[dc],
            aout[dc],eout[dc],fout[dc],fin[dc]);
        }
        
        fclose(outfile2);

    /* code to make plots with gnuplot */
        fp=popen("gnuplot -persist","w");
        fprintf(fp,"set terminal x11 \n");
        fprintf(fp,"set title 'Test iWave PLL Data'; set grid; set xlabel 'Time'; set ylabel 'Amplitude'; set zeroaxis \n");
        fprintf(fp,"plot 'results.txt' using 1:2 with lines title 'Data'\n");
        fprintf(fp,"set terminal postscript colour\n");
        fprintf(fp,"set output 'results.ps'; replot\n");
        fp=popen("gnuplot -persist","w");
        fprintf(fp,"set terminal x11\n");
        fprintf(fp,"set title 'Test iWave PLL Frequency'\n");
        fprintf(fp,"set grid; set xlabel 'Time'; set ylabel 'Frequency'\n");
        fprintf(fp,"plot 'results.txt' using 1:7 with lines title 'Inferred frequency'\n");
        fprintf(fp,"set terminal postscript colour; set output 'freq_results.ps'\n");
        fprintf(fp,"replot\n");
        fclose(fp);
        }

    return 0;
}