#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* calculate sum of squares of differences between observed and expected frequencies */
/* divided by expected frequency */
double chisquare(double fin[], double fout[], int nsamples) {
    int samplecount;
    double chisquared;

    for(samplecount=0; samplecount<nsamples; ++samplecount) {
        /* measure chiquared score */
        if(fin[samplecount]>0) {
            chisquared += (fout[samplecount]-fin[samplecount])*(fout[samplecount]-fin[samplecount])/fin[samplecount];
        }
    }
   
    return (double)chisquared;
}
